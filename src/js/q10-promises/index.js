/* eslint-disable */

/**
 * # Question 10 - Promises
 * Refer to README.md
 */


// Code your answer below

function delay (milliseconds) {
  return new Promise((resolve) => {
    const result = setTimeout(() => {
    	 resolve();
    }, milliseconds)
  });
}