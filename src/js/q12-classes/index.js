/**
 * # Question 12 - Classes
 *
 * Convert the following constructor function to a class
 */

const Person = function(first, last) {
  this.first = first;
  this.last = last;
};

Person.prototype.getName = function () {
  return this.first + ' ' + this.last;
};

// Converted:

class PersonClass {
    constructor(first, last) {
        this.first = first;
        this.last = last;
    }
}

let dave = new PersonClass('dave', 'taylor');

console.log(dave.first) // 'dave'
