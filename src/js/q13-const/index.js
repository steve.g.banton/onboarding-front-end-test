/* eslint-disable */

/**
 * # Question 13 - Const
 *
 */

const data = {
  name: 'Mc Jagger',
};

data.name = 'Mike Tyson';

// 13a - Why does it allow me to change `name` even though, data is a const ?

// ----------------------------------------------

data = {
  name: 'Michelle Obama',
};

// 13b - What will happen here, and why?


/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 13a
    Since data is an object, the object to which data is referring is not changing, just the contents.
  ## Answer 13b
    You'll get an error - you can't redefine the const variable.

*/
