/**
 * # Question 2 - Closure Counter
 * Refer to the README.md
 * 
 * 
 * 
 */
 
function makeCounter(initial) {
    this.count = initial;  
   	return {
    	count: () => {
      	return this.count
      },
    	increment: () => {
        this.count = this.count + 1;
    	},
      decrement: () => {
        this.count = this.count - 1;
    	},
    }
}

// Do not modify anything below this point
const counter1 = makeCounter();
const counter2 = makeCounter(10);

console.assert(counter1.count === 0);
console.assert(counter2.count === 10);

counter1.increment();
counter2.decrement();

console.assert(counter1.count === 1);
console.assert(counter2.count === 9);

counter1.count = 5;
counter2.count = 5;

console.assert(counter1.count === 1);
console.assert(counter2.count === 9);
