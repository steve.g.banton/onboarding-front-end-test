/* eslint-disable */

/**
 * # Question 4:
 *
 * What is the result of invoking the function `log`?
 * Why?
 */

log('Hello!');

const log = function (output) {
  console.log(output);
}


/*
  WRITE YOUR ANSWER HERE
  ----------------------
    const variables are not hoisted, so you'll get a reference error
*/
