/**
 * # Question 6:
 *
 * 1. What is the length of the Array?
 * 2. Why?
 * 3. What would be a better way to remove an item from an Array?
 */

const arr = [1, 2, 3];
delete arr[1];

/*
  WRITE YOUR ANSWER HERE
  ----------------------
  ## Answer 1
    3
  ## Answer 2
    arr[1] is simply emptied, not removed
  ## Answer 3
    arr.splice(1, 1);
*/
