/* eslint-disable */

/**
 * # Question 8 - Arrow Functions
 *
 * 1. Why does this example not work
 * 2. Why would a regular function fix it
 */

const person = {
  first: 'Jane',
  last: 'Doe',
  fullName: () => this.first + ' ' + this.last,
};

console.log(person.fullName());

/*
  WRITE YOUR ANSWER HERE
  ----------------------

  ## Answer 1
    The namespace is outside of person
  ## Answer 2
    A function takes its this from the function in which it is assigned.
*/
